from instagram_private_api import Client, ClientCompatPatch
from types import SimpleNamespace
import os
import csv

from dotenv import load_dotenv

from savesettings_logincallback import connect_with_settings

load_dotenv()


class Controller:

    def __init__(self, args):
        api = connect_with_settings(args)
        self.api = api

    def __handle_pagination(self, api_method, user_id, max_len, get_item_name):
        rank_token = Client.generate_uuid()

        results = []

        query_result = api_method(user_id=user_id, rank_token=rank_token)

        results.extend(query_result.get(get_item_name, []))

        next_max_id = query_result.get('next_max_id')

        while next_max_id:
            query_result = api_method(
                user_id=user_id, rank_token=rank_token, max_id=next_max_id)

            results.extend(query_result.get(get_item_name, []))

            print(f"{len(results)} of {max_len} fetched...")

            if len(results) >= max_len:
                break

            next_max_id = query_result.get('next_max_id')

        return results

    def export_followers(self, user_id, number_to_export):
        print(
            f"Started exporting {number_to_export} followers for user {user_id}")

        results = self.__handle_pagination(api_method=self.api.user_followers, user_id=user_id,
                                           max_len=number_to_export, get_item_name="users")

        try:
            with open("followers.csv", 'w') as outfile:
                outfile.write(",".join([item["username"] for item in results]))
        except TypeError:
            print("No results found. Please check the query method.")
            return

        print("Sucessfully fetched the followers. Hooray!")

    def search_for_follower(self, user_id, follower_username):
        rank_token = Client.generate_uuid()

        result = self.api.user_followers(
            user_id, rank_token, query=follower_username)

        does_user_exist = len(result.get("users", [])) > 0

        print(does_user_exist)

    def search_for_followers_in_csv(self, file_name, user_id):
        with open(file_name) as f:
            reader = csv.reader(f)
            followers = list(reader)
            followers = followers[0]

        for follower in followers:
            self.search_for_follower(user_id, follower)


args = {
    "username": os.getenv("USERNAME"),
    "password": os.getenv("PASSWORD"),
    "settings_file_path": "settings.json",
    "debug": False
}

# see: https://stackoverflow.com/a/16279578/10295948
args = SimpleNamespace(**args)

controller_object = Controller(args)
