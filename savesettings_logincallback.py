# see: https://instagram-private-api.readthedocs.io/en/latest/usage.html#avoiding-re-login

import json
import codecs
import datetime
import os.path
import logging
import argparse

from instagram_private_api import (
    Client, ClientError, ClientLoginError,
    ClientCookieExpiredError, ClientLoginRequiredError,
    __version__ as client_version)


def to_json(python_object):
    if isinstance(python_object, bytes):
        return {'__class__': 'bytes',
                '__value__': codecs.encode(python_object, 'base64').decode()}
    raise TypeError(repr(python_object) + ' is not JSON serializable')


def from_json(json_object):
    if '__class__' in json_object and json_object['__class__'] == 'bytes':
        return codecs.decode(json_object['__value__'].encode(), 'base64')
    return json_object


def onlogin_callback(api, new_settings_file):
    cache_settings = api.settings
    with open(new_settings_file, 'w') as outfile:
        json.dump(cache_settings, outfile, default=to_json)
        print('SAVED: {0!s}'.format(new_settings_file))


def reuse_settings(settings_file, args):
    with open(settings_file) as file_data:
        cached_settings = json.load(file_data, object_hook=from_json)
    print('Reusing settings: {0!s}'.format(settings_file))

    device_id = cached_settings.get('device_id')

    api = Client(
        args.username, args.password,
        settings=cached_settings)

    return api, device_id


def login_new_and_save_settings(args):
    api = Client(
        args.username, args.password,
        on_login=lambda x: onlogin_callback(x, args.settings_file_path))

    return api


def relogin_on_expired_cookie(args, device_id):
    # Login expired
    # Do relogin but use default ua, keys and such
    api = Client(
        args.username, args.password,
        device_id=device_id,
        on_login=lambda x: onlogin_callback(x, args.settings_file_path))

    return api


def set_logger():
    logging.basicConfig()
    logger = logging.getLogger('instagram_private_api')
    logger.setLevel(logging.WARNING)
    return logger


def connect_with_settings(args):
    logger = set_logger()

    if args.debug:
        logger.setLevel(logging.DEBUG)

    print('Client version: {0!s}'.format(client_version))

    device_id = None

    try:
        settings_file = args.settings_file_path
        does_settings_file_exist = os.path.isfile(settings_file)

        if not does_settings_file_exist:
            print('Unable to find file: {0!s}'.format(settings_file))

            api = login_new_and_save_settings(api)

        else:
            api, device_id = reuse_settings(settings_file, args)

    except (ClientCookieExpiredError, ClientLoginRequiredError) as e:
        print(
            'ClientCookieExpiredError/ClientLoginRequiredError: {0!s}'.format(e))
        api = relogin_on_expired_cookie(args, device_id)

    except ClientLoginError as e:
        print('ClientLoginError {0!s}'.format(e))
        exit(9)

    except ClientError as e:
        print('ClientError {0!s} (Code: {1:d}, Response: {2!s})'.format(
            e.msg, e.code, e.error_response))
        exit(9)

    except Exception as e:
        print('Unexpected Exception: {0!s}'.format(e))
        exit(99)

    # Show when login expires
    cookie_expiry = api.cookie_jar.auth_expires
    print('Cookie Expiry: {0!s}'.format(datetime.datetime.fromtimestamp(
        cookie_expiry).strftime('%Y-%m-%dT%H:%M:%SZ')))

    # Call the api
    results = api.user_feed('2958144170')
    assert len(results.get('items', [])) > 0

    print('Connection made successfully. Hooray!')

    return api


if __name__ == '__main__':
    connect_with_settings()
